# python-SQlite

## Summary

* Displaying SQlite database queries
* Importing Data from CSV

## Technologies used:

* python 2.7
* SQlite3

## Assignment

See the statement [here](https://github.com/jacquesberger/exemplesINF3005/tree/master/Ateliers/BD)

## License

This project is licensed under the Apache License - see [here](https://www.gnu.org/licenses/gpl-3.0.en.html) for details
